from django import forms
from django.contrib.auth.models import User

from administrator.models import Project


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = [
            'title',
            'url_address',
            'web_url',
        ]

    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        placeholders = {
            'title': 'Title',
            'url_address': 'Git repository URL',
            'web_url': 'Web URL',
            'proposed_by': 'Your name',
            'proposed_by_email': 'Your email'
        }
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            visible.field.widget.attrs['placeholder'] = placeholders[visible.name]
            visible.label = ''


class UserForm(forms.Form):
    username = forms.CharField(required=True, widget=forms.TextInput(
        attrs={
            "class": "form-control"
        }))
    email = forms.EmailField(required=True, widget=forms.EmailInput(
        attrs={
            "class": "form-control"
        }))
    password = forms.CharField(required=True, widget=forms.PasswordInput(attrs={
            "class": "form-control"
        }))
