from django.contrib import messages
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Count
from django.shortcuts import render, redirect

from administrator.forms import LoginForm
from osstracker.forms import ProjectForm, UserForm
from administrator.models import Project, Statistic, ProjectStatistic, Contributor, ProjectContributor
from services.GitVendors import GitVendor
import time


def home_view(request):
    return render(request, 'osstracker/home.html')


def statistics_view(request):
    statistic = Statistic.objects.latest('created_at')

    time_from_first_commit = int(statistic.time_from_first_commit / 60 / 60 / 24)
    average_commit_time = time.strftime('%H:%M:%S', time.gmtime(statistic.average_commit_time))
    first_contributor_joined_after = int(statistic.first_contributor_joined_after / 60 / 60 / 24)
    count_github = len(Project.objects.filter(git_vendor=GitVendor.GIT_VENDOR_GITHUB))
    count_gitlab = len(Project.objects.filter(git_vendor=GitVendor.GIT_VENDOR_GITLAB))

    return render(request, 'osstracker/statistics.html', {'statistic': statistic, 'time_from_first_commit': time_from_first_commit,
                                                          'average_commit_time': average_commit_time,
                                                          'first_contributor_joined_after': first_contributor_joined_after,
                                                          'count_github': count_github, 'count_gitlab': count_gitlab})


def tracked_repositories_view(request):
    contributor_search = request.GET.get('contributor_search')
    if contributor_search is not None:
        contributors = Contributor.objects.filter(name__icontains=contributor_search).all()

        sorting = ''
        projects = []

        for contributor in contributors:
            for project_contributor in contributor.projectcontributor_set.all():
                project = project_contributor.project
                if project not in projects:
                    projects.append(project_contributor.project)
    else:
        contributor_search = ''
        sorting = request.GET.get('sort', '-project_statistic__commits_count')

        projects = Project.objects.order_by(sorting).filter(is_approved=True)

    return render(request, 'osstracker/tracked_projects.html', {'projects': projects, 'can_change_status': False,
                                                                'sorting': sorting, 'contributor_search': contributor_search})


def project_local_statistic_view(request, id):
    project = Project.objects.get(pk=id)
    project_statistic = ProjectStatistic.objects.get(project=id)

    if project.proposed_by == request.user:
        project_statistic.viewed_by_proposer = True
        project_statistic.save()

    time_from_first_commit = int(project_statistic.time_from_first_commit / 60 / 60 / 24)
    average_commit_time = time.strftime('%H:%M:%S', time.gmtime(project_statistic.average_commit_time))

    first_contributor_joined_after = "This repository has no contributors"
    if project_statistic.first_contributor_joined_after is not None:
        first_contributor_joined_after = int(project_statistic.first_contributor_joined_after / 60 / 60 / 24)

    return render(request, 'osstracker/project_statistic.html', {'project': project, 'project_statistic': project_statistic,
                                                                 'time_from_first_commit': time_from_first_commit,
                                                                 'average_commit_time': average_commit_time,
                                                                 'first_contributor_joined_after': first_contributor_joined_after})


def project_contributors_statistic_view(request, id):
    project = Project.objects.get(pk=id)

    sorting = request.GET.get('sort', '-commit_count')

    contributors = ProjectContributor.objects.order_by(sorting).filter(project=id)

    extended = project.git_vendor == GitVendor.GIT_VENDOR_GITHUB

    return render(request, 'osstracker/contributors_statistic.html', {'project': project, 'contributors': contributors,
                                                                      'extended': extended, 'sorting': sorting})


def sign_in_view(request):
    message = ''

    if request.user.is_authenticated:
        return redirect('/home')

    if request.method == 'POST':
        form = LoginForm(request.POST)

        if not form.is_valid():
            return render(request, 'osstracker/sign_in.html', {'form': form})

        user = authenticate(
            request,
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password']
        )

        if user is not None:
            login(request, user)
            return redirect('/home')

        message = 'Invalid email or password'

    form = LoginForm()
    return render(request, 'osstracker/sign_in.html', {'form': form, 'message': message})


def sign_out_view(request):
    logout(request)

    return redirect('/sign_in')


def sign_up_view(request):
    if request.method == 'POST':
        form = UserForm(request.POST)

        if form.is_valid():
            user = User.objects.create_user(
                form.cleaned_data['username'],
                form.cleaned_data['email'],
                form.cleaned_data['password']
            )

            login(request, user)

            messages.success(request, 'Welcome to OSSTracker')
            return redirect('/home/')

    form = UserForm()
    return render(request, 'osstracker/sign_up.html', {'form': form})


@login_required(login_url='/sign_in')
def propose_repository_view(request):
    projects = Project.objects.filter(is_approved=True)
    git_vendor = GitVendor()

    if request.method == 'POST':
        form_project = ProjectForm(request.POST)
        if form_project.is_valid():
            project = form_project.save()
            project.git_vendor = git_vendor.parse_git_vendor_from_url(url=project.url_address)
            project.proposed_by = request.user
            project.save()

            messages.success(request, 'Project was successfully proposed.')
            return redirect('/tracked_repositories', {'form': form_project})
        else:
            return render(request, 'osstracker/propose_repository.html',
                          {'projects': projects, 'can_change_status': False, 'form': form_project})
    else:
        form_project = ProjectForm()
    return render(request, 'osstracker/propose_repository.html', {'projects': projects, 'form': form_project})


@login_required(login_url='/sign_in')
def proposed_repositories_view(request):
    projects = Project.objects.filter(proposed_by=request.user).all()

    return render(request, 'osstracker/my_repositories.html', {'projects': projects})
