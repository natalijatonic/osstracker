from typing import Final, Optional
from urllib.parse import urlparse
from django.conf import settings
from git import Commit
from github import Github
from gitlab import Gitlab
from administrator.models import Contributor, ContributorEmailHistory, Project


class GitVendor:
    GIT_VENDOR_GITHUB: Final = 'GitHub'
    GIT_VENDOR_GITLAB: Final = 'GitLab'

    @staticmethod
    def get_vendor(vendor: str) -> 'GitVendor':
        if vendor == GitVendor.GIT_VENDOR_GITHUB:
            return GitHub()

        return GitLab()

    def parse_git_vendor_from_url(self, url: str) -> str:
        parse_data = urlparse(url)
        git_vendor = parse_data.netloc
        if git_vendor == 'github.com':
            return self.GIT_VENDOR_GITHUB
        elif git_vendor == 'gitlab.com':
            return self.GIT_VENDOR_GITLAB

    def get_contributor_by_email(self, email: str) -> Optional[Contributor]:
        user = self.get_user_by_email(email)

        if user is None:
            self.store_unbound_email(email)
            return None

        return self.convert_user_to_contributor(user, email)

    def store_unbound_email(self, email: str):
        if ContributorEmailHistory.objects.filter(email_address=email).exists():
            return None

        email_history_log = ContributorEmailHistory()
        email_history_log.email_address = email
        email_history_log.save()

    def fallback_contributor(self, name: str, email: str) -> Contributor:
        contributor = Contributor()

        contributor.name = name
        contributor.email_address = email
        contributor.git_vendor = self.get_git_vendor()

        return contributor

    def get_user_by_email(self, email: str) -> object:
        pass

    def convert_user_to_contributor(self, user, email: str) -> Contributor:
        pass

    def get_commit_contributor(self, commit: Commit, project: Project) -> Optional[Contributor]:
        pass

    def get_git_vendor(self) -> str:
        pass

    def get_default_branch(self, project: Project) -> str:
        pass


class GitHub(GitVendor):
    def get_user_by_email(self, email: str) -> object:
        github = Github(login_or_token=settings.GITHUB_ACCESS_KEY)

        users = github.search_users(f'{email} in:email')

        if users.totalCount == 0:
            return None

        return users[0]

    def convert_user_to_contributor(self, user, email: str) -> Contributor:
        contributor = Contributor()

        contributor.email_address = email
        contributor.name = user.name
        contributor.image_url = user.avatar_url
        contributor.web_url = user.html_url

        if contributor.name is None:
            contributor.name = user.login

        contributor.location = user.location
        contributor.git_vendor = GitVendor.GIT_VENDOR_GITHUB

        return contributor

    def get_commit_contributor(self, commit: Commit, project: Project) -> Optional[Contributor]:
        github = Github(login_or_token=settings.GITHUB_ACCESS_KEY)

        repo = github.get_repo(self.get_repo_name_and_owner(project))
        github_commit = repo.get_commit(commit.hexsha)

        login = None
        if github_commit.committer is not None:
            login = github_commit.committer.login

        if github_commit.author is not None:
            login = github_commit.author.login

        if login is None:
            return self.fallback_contributor(commit.author.name, commit.author.email)

        author = github.get_user(login)  #login je ovde username

        return self.convert_user_to_contributor(author, commit.committer.email)

    def get_repo_name_and_owner(self, project: Project) -> str:
        return urlparse(project.url_address).path.strip('/')

    def get_git_vendor(self) -> str:
        return GitVendor.GIT_VENDOR_GITHUB

    def get_default_branch(self, project: Project) -> str:
        github = Github(login_or_token=settings.GITHUB_ACCESS_KEY)

        github_project = github.get_repo(self.get_repo_name_and_owner(project))

        return github_project.default_branch


class GitLab(GitVendor):
    def get_user_by_email(self, email: str) -> object:
        gitlab = Gitlab('http://gitlab.com', private_token=settings.GITLAB_ACCESS_KEY)

        users = gitlab.users.list(search=email)

        if len(users) == 0:
            return None

        user_id = users[0].get_id()

        return gitlab.users.get(user_id)

    def convert_user_to_contributor(self, user, email: str) -> Contributor:
        contributor = Contributor()

        contributor.email_address = email
        contributor.name = user.name
        contributor.location = user.location
        contributor.image_url = user.avatar_url
        contributor.git_vendor = GitVendor.GIT_VENDOR_GITLAB

        return contributor

    def get_commit_contributor(self, commit: Commit, project: Project) -> Optional[Contributor]:
        user = self.get_user_by_email(commit.author.email)

        if user is None:
            return self.fallback_contributor(commit.author.name, commit.author.email)

        return self.convert_user_to_contributor(user, commit.author.email)

    def get_repo_name_and_owner(self, project: Project) -> str:
        return urlparse(project.url_address).path.strip('/')

    def get_default_branch(self, project: Project) -> str:
        return 'master'

    def get_git_vendor(self) -> str:
        return GitVendor.GIT_VENDOR_GITLAB
