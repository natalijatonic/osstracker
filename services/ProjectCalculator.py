from services.ProjectStatisticsCalculators import *
from services.GitManager import GitManager


class ProjectCalculator:
    calculators = [CommitsCountCalculator, TagsCountCalculator, RepoInitializedFromCalculator, TimeFromFirstCommitCalculator,
                   CommitsPerTagCalculator]

    commit_calculators = [ContributorCountCalculator, AverageTimeCalculator, FirstContributorJoinedAfterCalculator,
                          SyncProjectContributors]

    def calculate_project_statistics(self, project: Project) -> ProjectStatistic:
        statistics = ProjectStatistic()
        statistics.project = project
        git_manager = GitManager()
        repo = git_manager.get_project_repo(project)
        commits = list(repo.iter_commits())
        commits.sort(key=lambda commit: commit.authored_datetime)

        contributor_repo = ContributorRepository()

        commit_calculators = self.get_commit_calculator_instances()
        for commit in commits:
            for calculator in commit_calculators:
                calculator.calculate_for_commit(project, commit, contributor_repo)

        for calculator in commit_calculators:
            calculator.apply_to_statistic(statistics)

        for calculator in self.calculators:
            calculator_instance = calculator()
            calculator_instance.calculate(project, repo, commits, contributor_repo, statistics)

        return statistics

    def get_commit_calculator_instances(self) -> list[Calculator]:
        instances = []
        for calculator in self.commit_calculators:
            instances.append(calculator())

        return instances
