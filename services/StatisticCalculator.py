from services.GlobalStatisticsCalculators import *


class StatisticCalculator:
    calculators = [LocalStatisticsAggregateCalculator, ProjectsCountCalculator]

    def calculate(self) -> Statistic:
        statistics = Statistic()

        for calculator in self.calculators:
            calculator_instance = calculator()
            calculator_instance.calculate(statistics)

        return statistics
