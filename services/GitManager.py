import stat

from git import Repo, Git
from administrator.models import Project
import os
import shutil

from services.GitVendors import GitVendor


class GitManager:
    repoPaths = 'C:\\Users\\Hp\\Desktop\\Diplomski\\OSSTracker\\repos'

    def get_repo_local_path(self, project: Project) -> str:
        return self.repoPaths + '\\project-' + str(project.pk)

    def init_project_repo(self, project: Project) -> str:
        target_dir = self.get_repo_local_path(project)
        os.mkdir(target_dir)
        origin_url = project.url_address
        os.system(self.prepare_clone_command(origin_url, target_dir))
        self.sync_main_branch(project)
        self.sync_latest_commit_hash(project)
        self.sync_project_author(project)

        return target_dir

    def delete_project_repo(self, project: Project):
        target_dir = self.get_repo_local_path(project)
        for root, dirs, files in os.walk(target_dir):
            for momo in dirs:
                os.chmod(os.path.join(root, momo), stat.S_IWUSR)
            for momo in files:
                os.chmod(os.path.join(root, momo), stat.S_IWUSR)
        shutil.rmtree(target_dir)

    def prepare_clone_command(self, originUrl: str, targetDir: str):
        return f'git clone {originUrl} {targetDir} --bare'

    def get_project_repo(self, project: Project) -> Repo:
        return Repo(self.get_repo_local_path(project))

    def get_project_commits(self, project: Project):
        return list(self.get_project_repo(project).iter_commits(project.default_branch))

    def sync_main_branch(self, project: Project):
        vendor = GitVendor.get_vendor(project.git_vendor)

        project.default_branch = vendor.get_default_branch(project)
        project.save()

    def sync_latest_commit_hash(self, project: Project):
        repo = self.get_project_repo(project)
        commit = list(repo.iter_commits())[0]
        project.latest_commit_hash = commit.hexsha
        project.save()

    def sync_project_author(self, project: Project):
        first_commit = self.get_project_commits(project)[0]

        project.author_name = first_commit.author.name
        project.author_email = first_commit.author.email

        project.save()
