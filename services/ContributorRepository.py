from typing import Optional

from git import Commit

from administrator.models import Contributor, Project
from services.GitVendors import GitVendor, GitHub, GitLab


class ContributorRepository:
    contributors: dict[str, Contributor] = {}

    def get_commit_contributor(self, commit: Commit, git_vendor: str, project: Project) -> Optional[Contributor]:
        contributor = self.get_by_email(commit.committer.email, git_vendor)

        if contributor is not None:
            return contributor

        vendor = self.get_git_vendor(git_vendor)

        contributor = vendor.get_commit_contributor(commit, project)
        contributor.save()
        self.contributors[contributor.email_address] = contributor

        return contributor

    def get_by_email(self, email: str, git_vendor: str) -> Optional[Contributor]:
        if email in self.contributors:
            return self.contributors[email]

        contributor = Contributor.objects.filter(email_address=email).first()

        if contributor is None:
            vendor = self.get_git_vendor(git_vendor)
            contributor = vendor.get_contributor_by_email(email)

            if contributor is None:
                return None

            contributor.save()

        self.contributors[contributor.email_address] = contributor
        return contributor

    def get_git_vendor(self, vendor_name: str) -> GitVendor:
        return GitVendor.get_vendor(vendor_name)
