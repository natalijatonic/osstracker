from abc import abstractmethod
from django.db.models import Avg, Count

from administrator.models import Statistic, ProjectStatistic, Project


class GlobalCalculator:

    @classmethod
    @abstractmethod
    def calculate(cls,  statistics: Statistic):
        pass


class LocalStatisticsAggregateCalculator(GlobalCalculator):
    def calculate(cls,  statistics: Statistic):
        statistics.contributor_count = ProjectStatistic.objects.aggregate(Avg('contributor_count'))['contributor_count__avg']
        statistics.average_commit_time = ProjectStatistic.objects.aggregate(Avg('average_commit_time'))['average_commit_time__avg']
        statistics.time_from_first_commit = ProjectStatistic.objects.aggregate(Avg('time_from_first_commit'))['time_from_first_commit__avg']
        statistics.commits_count = ProjectStatistic.objects.aggregate(Avg('commits_count'))['commits_count__avg']
        statistics.first_contributor_joined_after = ProjectStatistic.objects.aggregate(Avg('first_contributor_joined_after'))['first_contributor_joined_after__avg']
        statistics.tag_count = ProjectStatistic.objects.aggregate(Avg('tag_count'))['tag_count__avg']
        statistics.commits_per_tag = ProjectStatistic.objects.aggregate(Avg('commits_per_tag'))['commits_per_tag__avg']
        statistics.most_repos_created_from = ProjectStatistic.objects.values("location").annotate(created_from=Count('location')).order_by('-created_from')[0]["location"]


class ProjectsCountCalculator(GlobalCalculator):
    def calculate(cls,  statistics: Statistic):
        statistics.projects_count = Project.objects.filter(is_approved=True).count()

