from administrator.models import ProjectStatistic, ProjectContributor
from git import Commit, Repo
from services.ContributorRepository import *


class Calculator:
    def calculate(cls, project: Project, repo: Repo, commits: list[Commit], contributor_repository: ContributorRepository, project_statistic: ProjectStatistic):
        pass

    def calculate_for_commit(self, project: Project, commit: Commit, contributor_repository: ContributorRepository):
        pass

    def apply_to_statistic(self, statistic: ProjectStatistic):
        pass

    def average(self, list: list) -> float:
        return sum(list) / len(list)


class ContributorCountCalculator(Calculator):
    authors = []

    def __init__(self):
        self.authors = []

    def calculate_for_commit(self, project: Project, commit: Commit, contributor_repository: ContributorRepository):
        if commit.author.email in self.authors:
            return

        self.authors.append(commit.author.email)

    def apply_to_statistic(self, statistic: ProjectStatistic):
        statistic.contributor_count = len(self.authors)


class SyncProjectContributors(Calculator):
    contributors = dict()

    def __init__(self):
        self.contributors = dict()
    
    def calculate_for_commit(self, project: Project, commit: Commit, contributor_repository: ContributorRepository):
        contributor = contributor_repository.get_commit_contributor(commit, project.git_vendor, project)

        if contributor is None:
            return

        if contributor.email_address not in self.contributors:
            self.contributors[contributor.email_address] = {
                'contributor': contributor,
                'commits_count': 0
            }

        self.contributors[contributor.email_address]['commits_count'] += 1

    def apply_to_statistic(self, statistic: ProjectStatistic):
        project = statistic.project

        ProjectContributor.objects.filter(project=project).delete()

        for contributor_email in self.contributors:
            project_contributor = ProjectContributor()
            project_contributor.project = project
            project_contributor.contributor = self.contributors[contributor_email]['contributor']
            project_contributor.commit_count = self.contributors[contributor_email]['commits_count']
            project_contributor.save()


class AverageTimeCalculator(Calculator):
    committed_at_seconds = []

    def __init__(self):
        self.committed_at_seconds = []

    def calculate_for_commit(self, project: Project, commit: Commit, contributor_repository: ContributorRepository):
        committed_at = commit.authored_datetime
        committed_date_start = committed_at.replace(hour=0, minute=0, second=0, microsecond=0)
        self.committed_at_seconds.append((committed_at - committed_date_start).total_seconds())

    def apply_to_statistic(self, statistic: ProjectStatistic):
        statistic.average_commit_time = self.average(self.committed_at_seconds)


class TimeFromFirstCommitCalculator(Calculator):
    def calculate(cls, project: Project, repo: Repo, commits: list[Commit],  contributor_repository: ContributorRepository, project_statistic: ProjectStatistic):
        project_statistic.time_from_first_commit = (commits[-1].authored_datetime - commits[0].authored_datetime).total_seconds()


class CommitsCountCalculator(Calculator):
    def calculate(cls, project: Project, repo: Repo, commits: list[Commit],  contributor_repository: ContributorRepository, project_statistic: ProjectStatistic):
        project_statistic.commits_count = len(commits)


class FirstContributorJoinedAfterCalculator(Calculator):
    author = None
    initialized_at = None
    joined_after = None

    def __init__(self):
        self.author = None
        self.initialized_at = None
        self.joined_after = None

    def calculate_for_commit(self, project: Project, commit: Commit, contributor_repository: ContributorRepository):
        if self.joined_after is not None:
            return

        if self.author is None:
            self.author = commit.author.email
            self.initialized_at = commit.authored_datetime
            return

        if self.author == commit.author.email:
            return

        self.joined_after = (commit.authored_datetime - self.initialized_at).total_seconds()

    def apply_to_statistic(self, statistic: ProjectStatistic):
        statistic.first_contributor_joined_after = self.joined_after


class TagsCountCalculator(Calculator):
    def calculate(cls, project: Project, repo: Repo, commits: list[Commit], contributor_repository: ContributorRepository, project_statistic: ProjectStatistic):
        project_statistic.tag_count = len(repo.tags)


class RepoInitializedFromCalculator(Calculator):
    def calculate(cls, project: Project, repo: Repo, commits: list[Commit], contributor_repository: ContributorRepository, project_statistic: ProjectStatistic):
        author = contributor_repository.get_commit_contributor(commits[0], project.git_vendor, project)
        if author is None:
            return None
        project_statistic.location = author.location


class CommitsPerTagCalculator(Calculator):
    def calculate(self, project: Project, repo: Repo, commits: list[Commit], contributor_repository: ContributorRepository, project_statistic: ProjectStatistic):
        tags = repo.tags

        if len(tags) == 0:
            project_statistic.commits_per_tag = len(commits)
            return

        current_tag = 0
        commits_per_tag = []
        current_tag_commits = 0

        for commit in commits:
            current_tag_commits += 1
            if tags[current_tag].commit.hexsha == commit.hexsha:
                commits_per_tag.append(current_tag_commits)
                current_tag_commits = 0
                current_tag += 1

        if current_tag_commits > 0:
            commits_per_tag.append(current_tag_commits)

        project_statistic.commits_per_tag = self.average(commits_per_tag)


