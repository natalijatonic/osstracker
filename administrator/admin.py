from django.contrib import admin

from administrator.models import Contributor, Project, ProjectContributor, ProjectStatistic, Statistic, ContributorEmailHistory

admin.site.register(Contributor)
admin.site.register(Project)
admin.site.register(ProjectContributor)
admin.site.register(ProjectStatistic)
admin.site.register(Statistic)
admin.site.register(ContributorEmailHistory)
