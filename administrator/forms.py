from django import forms
from django.contrib.auth import get_user_model
from administrator.models import Project

User = get_user_model()


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(
        attrs={
            "class": "form-control"
        }))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={
            "class": "form-control",
            "id": "user-password"
        }
    )
    )


def clean_username(self):
    username = self.cleaned_data.get("username")
    qs = User.objects.filter(username__iexact=username)
    if not qs.exists():
        raise forms.ValidationError("This is an invalid user.")
    if qs.count() != 1:
        raise forms.ValidationError("This is an invalid user.")
    return username


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = [
            'title',
            'url_address',
            'web_url',
        ]

    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        placeholders = {
            'title': 'Title',
            'url_address': 'Git repository URL',
            'web_url': 'Web URL',

        }
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            visible.field.widget.attrs['placeholder'] = placeholders[visible.name]
            visible.label = ''
