from django.contrib.auth.models import User
from django.db import models
from abc import abstractmethod, ABC
from django.utils.timezone import now


class BaseModel(models.Model):

    @classmethod
    def create(cls, *args, **kwargs):
        pass

    @classmethod
    def edit(cls, *args, **kwargs):
        pass

    class Meta:
        abstract = True


class Project(BaseModel):
    title = models.CharField(max_length=50)
    author_name = models.CharField(max_length=200, blank=True)
    author_email = models.CharField(max_length=200, blank=True)
    url_address = models.CharField(max_length=100)
    git_vendor = models.CharField(max_length=10, blank=True)
    is_approved = models.BooleanField(null=True, default=None)
    is_initial = models.BooleanField(null=False, blank=True, default=False)
    web_url = models.CharField(max_length=100, blank=True)
    latest_commit_hash = models.CharField(max_length=40)
    default_branch = models.CharField(max_length=100, null=True, default=None)
    proposed_by = models.ForeignKey(User, models.SET_NULL, null=True)
    approved_at = models.DateTimeField(null=True, default=None)

    def __str__(self):
        return self.title

    @classmethod
    def create(cls, title, author, url_address, git_vendor, is_approved, web_url, proposed_by, proposed_by_email,
               is_initial=False):
        project = cls(title=title, author=author, url_address=url_address, git_vendor=git_vendor,
                      is_approved=is_approved, is_initial=is_initial, web_url=web_url, proposed_by=proposed_by,
                      proposed_by_email=proposed_by_email)
        return project

    @classmethod
    def edit(cls, new_title, new_git_url, new_web_url):
        project = cls(title=new_title, git_url=new_git_url, web_url=new_git_url)
        return project


class Contributor(BaseModel):
    email_address = models.CharField(max_length=300)
    name = models.CharField(max_length=50)
    location = models.CharField(max_length=50, blank=True, null=True)
    git_vendor = models.CharField(max_length=10)
    web_url = models.URLField(null=True)
    image_url = models.URLField(null=True)

    @classmethod
    def create(cls, name, email_address, location, git_vendor):
        author = cls(name=name, email_address=email_address, location=location, git_vendor=git_vendor)
        return author

    def __str__(self):
        return self.name


class ProjectContributor(BaseModel):
    project = models.ForeignKey(Project, blank=False, on_delete=models.CASCADE)
    contributor = models.ForeignKey(Contributor, blank=False, on_delete=models.RESTRICT)
    commit_count = models.IntegerField()


class ProjectStatistic(models.Model):
    project = models.OneToOneField(Project, blank=False, on_delete=models.CASCADE, related_name='project_statistic')
    contributor_count = models.IntegerField()
    average_commit_time = models.IntegerField()
    time_from_first_commit = models.IntegerField()
    commits_count = models.IntegerField()
    first_contributor_joined_after = models.IntegerField(blank=True, null=True)
    tag_count = models.IntegerField()
    commits_per_tag = models.IntegerField()
    location = models.CharField(max_length=30, null=True, blank=True)
    viewed_by_proposer = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=now)


class Statistic(models.Model):
    contributor_count = models.FloatField()
    average_commit_time = models.IntegerField()
    time_from_first_commit = models.IntegerField()
    commits_count = models.FloatField()
    first_contributor_joined_after = models.IntegerField(blank=True)
    tag_count = models.FloatField()
    commits_per_tag = models.FloatField()
    most_repos_created_from = models.CharField(max_length=30, null=True, blank=True)
    created_at = models.DateTimeField(default=now)
    projects_count = models.IntegerField()


class ContributorEmailHistory(models.Model):
    contributor = models.ForeignKey(Contributor, blank=True, null=True, on_delete=models.CASCADE)
    email_address = models.CharField(max_length=300)

