from django.http import HttpResponseRedirect
from django.contrib import messages
from django.shortcuts import render, redirect
from administrator.forms import LoginForm, ProjectForm
from django.contrib.auth import authenticate, login, logout
from administrator.models import Project, ProjectStatistic
from django.db.models import Q
from django.contrib.admin.views.decorators import staff_member_required
from datetime import datetime

from services.GitManager import GitManager


def login_view(request):
    if request.user.is_authenticated:
        return redirect('/admin/repositories/tracked')

    form = LoginForm(request.POST or None)
    message = None
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/admin/repositories/tracked')
        else:
            message = 'Incorrect username or password, try again.'
            request.session['invalid_user'] = 1 # 1 == True
    return render(request, "admin/login.html", {"form": form, 'message': message})


@staff_member_required(login_url='/admin/login')
def logout_view(request):
    logout(request)
    return redirect('/admin/login')


@staff_member_required(login_url='/admin/login')
def proposed_projects(request):
    projects = Project.objects.filter(is_approved=None)

    return render(request, 'admin/projects_list.html', {'projects': projects, 'title': 'Proposed Repositories', 'actions': 'approve/decline'})


@staff_member_required(login_url='/admin/login')
def tracked_projects(request):
    projects = Project.objects.filter(Q(is_approved=True) | Q(is_initial=True))

    return render(request, 'admin/projects_list.html', {'projects': projects, 'title': 'Accepted Repositories', 'actions': 'edit'})

@staff_member_required(login_url='/admin/login')
def declined_projects(request):
    projects = Project.objects.filter(is_approved=False)

    return render(request, 'admin/projects_list.html', {'projects': projects, 'title': 'Declined Repositories', 'actions': 'approve/delete'})


@staff_member_required(login_url='/admin/login')
def edit_tracked_project(request, id):
    project = Project.objects.get(pk=id)

    if request.method == 'POST':
        old_git_url = project.url_address
        form = ProjectForm(request.POST, instance=project)
        if not form.is_valid():
            messages.success(request, 'Form is not valid, please fill all the fields correctly')
            return render(request, 'admin/edit_project.html', {'form': form, 'project': project, 'action': 'delete'})
        form.save()

        if project.url_address != old_git_url:
            git_manager = GitManager()
            git_manager.delete_project_repo(project)
            git_manager.init_project_repo(project)
            ProjectStatistic.objects.filter(project=project).delete()

        messages.success(request, 'Changes saved.')
        return redirect('/admin/repositories/tracked')
    else:
        form = ProjectForm(instance=project)
    return render(request, 'admin/edit_project.html', {'form': form, 'project': project, 'action': 'delete'})


@staff_member_required(login_url='/admin/login')
def approve_project(request, id):
    project = Project.objects.get(pk=id)
    project.is_approved = True

    project.approved_at = datetime.now()
    project.save(update_fields=['is_approved', 'approved_at'])

    manager = GitManager()
    manager.init_project_repo(project)

    messages.success(request, "Project successfully approved")

    return redirect('/admin/repositories/proposed')


@staff_member_required(login_url='/admin/login')
def decline_project(request, id):
    project = Project.objects.get(pk=id)
    project.is_approved = False
    project.save(update_fields=['is_approved'])

    messages.success(request, 'Project successfully declined')

    return redirect('/admin/repositories/proposed')


@staff_member_required(login_url='/admin/login')
def delete_project_view(request, id):
    action = 'delete'
    project = Project.objects.get(pk=id)
    project.delete()
    messages.success(request, 'Project was successfully deleted.')

    if action:
        return redirect('/admin/repositories/tracked')

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

