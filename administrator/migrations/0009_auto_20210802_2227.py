# Generated by Django 3.1.4 on 2021-08-02 20:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('administrator', '0008_auto_20210802_2220'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projectstatistic',
            name='location',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='statistic',
            name='location',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
    ]
