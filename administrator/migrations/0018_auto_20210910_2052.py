# Generated by Django 3.1.4 on 2021-09-10 18:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('administrator', '0017_auto_20210825_2356'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contributor',
            name='email_address',
            field=models.CharField(max_length=300),
        ),
    ]
