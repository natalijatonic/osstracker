# Generated by Django 3.1.4 on 2021-08-25 21:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('administrator', '0016_auto_20210825_2233'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectContributor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('commit_count', models.IntegerField()),
                ('contributor', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='administrator.contributor')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='project',
            name='author',
        ),
        migrations.DeleteModel(
            name='Commit',
        ),
        migrations.AddField(
            model_name='projectcontributor',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='administrator.project'),
        ),
    ]
