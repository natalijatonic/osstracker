from django.core.management.base import BaseCommand
from administrator.models import Project, ProjectStatistic, Statistic
from services.GitManager import GitManager
from services.ProjectCalculator import ProjectCalculator
from services.StatisticCalculator import StatisticCalculator


class Command(BaseCommand):
    def handle(self, *args, **options):
        projects = Project.objects.filter(is_approved=True)
        git_manager = GitManager()
        project_calculator = ProjectCalculator()
        statistic_calculator = StatisticCalculator()
        change_detected = len(projects) != Statistic.objects.latest('created_at').projects_count

        for project in projects:
            print("Processing ", project.title)
            repo = git_manager.get_project_repo(project)
            if not ProjectStatistic.objects.filter(project=project).exists():
                project_calculator.calculate_project_statistics(project).save()
                change_detected = True
            else:
                remote = repo.remotes.origin
                remote.fetch("+refs/heads/*:refs/heads/*")
                commits = list(repo.iter_commits())
                commit = commits[-1]
                commit_hash = commit.hexsha
                if project.latest_commit_hash is not commit_hash:
                    ProjectStatistic.objects.filter(project=project).delete()
                    project_calculator.calculate_project_statistics(project).save()
                    change_detected = True

        if change_detected:
            statistic_calculator.calculate().save()
























