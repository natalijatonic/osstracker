"""osstracker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path

from osstracker.views import (
    home_view,
    statistics_view,
    tracked_repositories_view,
    project_local_statistic_view,
    project_contributors_statistic_view,
    sign_in_view,
    sign_out_view,
    sign_up_view,
    propose_repository_view,
    proposed_repositories_view,
)

from administrator.views import (
    login_view,
    logout_view,
    tracked_projects,
    proposed_projects,
    declined_projects,
    edit_tracked_project,
    approve_project,
    decline_project,
    delete_project_view,
)

urlpatterns = [
    path('django-administration/', admin.site.urls),
    path('home/', home_view),
    path('statistics/', statistics_view),
    path('project_statistic/<int:id>/', project_local_statistic_view),
    path('contributor_statistic/<int:id>/', project_contributors_statistic_view),
    path('tracked_repositories/', tracked_repositories_view),
    path('sign_in/', sign_in_view),
    path('sign_out/', sign_out_view),
    path('sign_up/', sign_up_view),
    path('propose_repository/', propose_repository_view),
    path('proposed_repositories/', proposed_repositories_view),
    path('admin/login/', login_view),
    path('admin/logout/', logout_view),
    path('admin/repositories/proposed', proposed_projects),
    path('admin/repositories/declined', declined_projects),
    path('admin/repositories/tracked', tracked_projects),
    path('admin/repositories/<int:id>/edit', edit_tracked_project),
    path('admin/repositories/<int:id>/approve', approve_project),
    path('admin/repositories/<int:id>/decline', decline_project),
    path('admin/repositories/<int:id>/delete', delete_project_view),
]
